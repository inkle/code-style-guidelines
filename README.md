# inkle code style guidelines

These guidelines currently cover Objective C only.

## Commenting and file layout

 - Header file for a class should contain an overview description of its purpose in the top comment block:

 
	    //
	    //  MyObject.h
	    //  Project Name
	    //
	    //  Created by Joseph Humfrey on 22/01/2014.
	    //  Copyright (c) 2014 inkle Ltd. All rights reserved.
	    //
	    // The description of the class you're building goes here
	    
	    
 - Separators: Use the following code snippet above class interface/implementation and above groups of related method definitions:
 
		//------------------------------------------------------------------------------------
		#pragma mark - Add a title here
		//------------------------------------------------------------------------------------
		
	The `#pragma mark` creates section headers in the dropdown at the top of the code editor.
	
 - Constants, 'magic numbers', private struct and enum definitions etc, all go above the main class `#pragma mark` separator
	
## Curly brackets

Opening curly bracket should be on:

 - Next line for method definitions
 - Same line for everything else:
 
		- (id)initWithFrame:(CGRect)frame
		{
		    self = [super initWithFrame:frame];
		    if (self) {
		    
		    }
		    return self;
		}
		
 - Never omit curly brackets for `if`, `for`, `while`, etc:

	    if ( something ) {
	        return;
	    }
	    
## Properties, ivars, variables

 - ivar declarations always go in implementation file, **NOT** in interface file:
 
		@implementation SomeView {
		    NSMutableArray      *_anArray;
		}
		
 - Prefer ivars over private properties in class extension for general usage.

 - Magic numbers, constants etc, should be prefixed by `k`. E.g.:
 
	 	static const float kTopMargin = 40;
	 	
 - Use typed constants as above. Do not use macros for simple value constants. e.g. Avoid: `#define TOP_MARGIN 40`.	
